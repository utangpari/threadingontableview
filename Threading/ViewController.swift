//
//  ViewController.swift
//  Threading
//
//  Created by pari on 06/10/17.
//  Copyright © 2017 pari. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView : UITableView!
    var users = [String]()
    let groupA = ["user1","user2"]
    let groupB = ["user3","user4"]
    let groupC = ["user5","user6"]
    let dispatchGroup = DispatchGroup()
    func run(after seconds:Int, completion: @escaping() -> Void ){
        let deadline = DispatchTime.now() + .seconds(seconds)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            completion()
        }
    }
    func getGroupA(){
        dispatchGroup.enter()
        run(after: 3) {
            print("Got A")
            self.users.append(contentsOf: self.groupA)
            self.dispatchGroup.leave()
        }
    }
    func getGroupB(){
        dispatchGroup.enter()
        run(after: 4) {
            print("Got B")
            self.users.append(contentsOf: self.groupB)
            self.dispatchGroup.leave()
        }
    }
    func getGroupC(){
        dispatchGroup.enter()
        run(after: 6) {
            print("Got C")
            self.users.append(contentsOf: self.groupC)
            self.dispatchGroup.leave()
        }
    }
    func displayUsers(){
        print("reloading data")
        tableView.reloadData()
    }
    @IBAction func onDownloadTapped(){
        print("Downloading...")
        getGroupA()
        getGroupB()
        getGroupC()
        dispatchGroup.notify(queue: .main) {
            self.displayUsers()
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

    }
}

extension ViewController : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Reuse Identifier")
        cell.textLabel?.text = users[indexPath.row]
        return cell
    }
}

